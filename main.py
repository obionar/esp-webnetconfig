import time
import network
import ujson
import urequests
from machine import Pin



ap = network.WLAN(network.AP_IF)
sta = network.WLAN(network.STA_IF)
sta.active(True)


### IF STATEMENT HERE:



web_enabled = False
# run your main program here
while not web_enabled:
	# urequests module blocks your program
	# so you need to set 'wifi_enabled = True'
	# with external gpio button or another method
	
	
	pass
	

#### WIFI CONFIG SECTION ####

# READ WIFI SETTINGS FROM FILE
def json_read():
    global essid, key 
    with open('wifi.json', 'r') as f:
        s = ujson.load(f)
        essid = s['essid']
        key = s['key']

json_read()


def do_connect():
    json_read()
    sta.connect(essid, key)
    # WAIT FOR CONNECTION
    timeout = 10
    while not sta.isconnected() and timeout > 0:
        print('connecting')
        led.off()
        time.sleep(.5)
        led.on()
        time.sleep(.5)
        timeout -= 1
        print(timeout)

    if sta.isconnected():
        led.on()

        print('connected to:', essid)

    else:
        led.off()
        print('connection failed!')

    print('ifconfig:', sta.ifconfig())

# WIFI RANGE SCAN
def wifi_scan():
    sta.active()
    wifi_list = []
    for i in sta.scan():
        s_ap = []
        scan_essid = str(i[0], 'utf-8')
        signal = i[3]
        s_ap = [scan_essid, signal]
        wifi_list.append(s_ap)
    return wifi_list


# TRY TO CONNECT 
if not sta.isconnected():
    print(essid, 'found')
    do_connect()
elif sta.isconnected():
    print('connected')
    
else:
    web_enabled = True
    print(essid, 'not in range')



# WEB INTERFACE MODE
if web_enabled:
    import usocket as socket
    
    #START AP
    ap.active(True)
    ap.config(essid='Wemos2')
    ap.config(authmode=0)#, password='password')
    print(ap.ifconfig())


    print('\n\n')
    print('starting web server')

    my_socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    my_socket.bind(('',80))
    my_socket.listen(1)
    
    while web_enabled:
        connection,address = my_socket.accept()
        request = connection.recv(1024).decode('utf-8')
        string_list = request.split(' ')     # Split request from spaces
        get_req = str(string_list[1])
        print(get_req)
        
        
        
        html = '''
                <a href="/">Status</a> |
                <a href="/wifi">WiFi Settings</a> |
                '''
        if get_req == '/':
            adc = ADC(0).read()
            battery = adc * 3.3 / 1024 * 3
            battery = round(battery, 1)
            distance = 333.0
            percent = 100
            
            if sta.isconnected():
                wifi = '{} ({}db)'.format(essid, sta.status('rssi'))
            
            else:
                wifi = 'disconnected'
            
            
            html += '''
                <h2>Device status</h2>
                <table border="1">
                    <tr>
                        <td>Wifi</td> <td>{}</td>
                    </tr>
                </table>
                

            '''.format(dev_id, wifi, distance, percent, battery)

        elif get_req == '/wifi':
            html += '<h2>Network Configuration</h2>'
            
            if not sta.isconnected():
                print('not connected')
        
                option_essid = ''
                for i in wifi_scan():
                    print(i)
                    option_essid += '<option value="{}">{} {} db</option>'.format(i[0], i[0], i[1])       
                
                html += '''
                    <h3> connect to wifi: </h3>
                    <form action="/connect">
                        Select Network:<br>
                        <select name="essid">
                            {}
                        </select><br>
                        Password:<br>
                        <input type="password" name="key">
                        <br>
                        <input type="submit" value="Save & Connect"><br>
                    </form>
                '''.format(option_essid)
        
            elif sta.isconnected():
                html += 'Network Configured and Connected to: ' + essid
                
                html += '<a href="/disconnect"><br>(Disconnect and forget)</a>'
        
        elif get_req.split('?')[0] == "/connect":
            html += '<a href="/connect"> Refresh </a>'
            
            if not sta.isconnected():
                get_wifi = get_req.split('?')[1].split('&')
                new_essid = get_wifi[0].split('=')[1]
                new_key = get_wifi[1].split('=')[1]
                new_wifi = {'essid':new_essid, 'key':new_key}
                with open('wifi.json', 'w') as wf:
                    wf.write(ujson.dumps(new_wifi))
                do_connect()
                

            elif sta.isconnected():
                html += "Connection Successful!"

        elif get_req == "/disconnect":
            print('disconnecting')
            sta.disconnect()
            zero_pass = {'essid':'zero', 'key':'zero'}
            with open('wifi.json', 'w') as wf:
                wf.write(ujson.dumps(zero_pass))

            time.sleep(1)
            html += "disconnected"
        
                    
        response = ''' 
                        <html>
                            <head>
                                <title>title</title>
                                
                                <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
                                
                                <style>
                                    .container {
                                    font: 1.2em Arial;
                                    max-width: 99%;
                                    margin: 1em auto;
                                    padding: 1em;
                                }
                                </style>
                            </head>
                                <body>
                                
                                <div class="container">
                                    <h2>Device Configuration Page</h2>
                                    ''' + html + '''
                                </div>
                                </body>
                        </html>
                        '''

        response = response.encode('utf-8')
        
        connection.send('HTTP/1.1 200 OK\n')
        connection.send('Content-Type: text/html\n')
        connection.send('Connection: close\n\n')
        connection.sendall(response)
        
        connection.close()
                    

